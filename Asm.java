
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import asm.code.Code;
import asm.code.SemanticError;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;
import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 * Podporni razred za predmet Sistemska programska oprema.
 *
 * @author jure
 */
public class Asm {

    public static String readFile(File file) {
        byte[] buf = new byte[(int) file.length()];
        try {
            InputStream s = new FileInputStream(file);
            try {
                s.read(buf);
            } finally {
                s.close();
            }
        } catch (IOException e) {
            System.err.format("Could not read file: %s\n",e.toString());
            return "";
        }
        return new String(buf);
    }

    public static void main(String[] args) {
        
        if(args.length>0) {
            File inputFile = new File(args[0]);
            String input = readFile(inputFile);

            Parser parser = new Parser();
            Code code;
            try {
                code = parser.parse(input);
                //code.print();
                
                StringBuilder sb = new StringBuilder();
                code.activate();
                code.resolve();
                sb.append("\n*** listing ***\n");
                code.generateListing(sb);
                sb.append("\n*** symbols ***\n");
                code.dumpSymbols(sb);
                sb.append("\n*** object ***\n");
                StringBuilder sbObj = new StringBuilder();
                code.emitText(sbObj);
                
                sb.append(sbObj);
                System.out.println(sb);
                
                File objectFile = new File(inputFile.getParentFile(),
                        inputFile.getName().substring(0, inputFile.getName().indexOf('.'))+".obj");
                
                try (BufferedWriter output = new BufferedWriter(new FileWriter(objectFile))) {
                    output.write(sbObj.toString());
                    output.flush();
                }
                catch (IOException e) {
                    System.err.format("Failed to write to object file [%s]:%s\n",
                            objectFile.getAbsolutePath(),
                            e.toString());
                }
            } catch (SyntaxError | SemanticError e) {
                System.err.println(e);
                e.printStackTrace();
                System.exit(1);
            }
        }
        else
            System.out.println("Usage: Asm [path to assembly source file]");
    }

}
