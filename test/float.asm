float	START   0
first	LDA		#3
		FLOAT
		STF		a
		LDA		#1
		FLOAT
		DIVF	a
		STF		a
		
		LDA		#4
		FLOAT
		STF		b
		LDA		#1
		FLOAT
		DIVF	b
		STF		b
		
. sum
		LDF     a
		ADDF	b
		STF		sum
. diff
		LDF     a
		SUBF	b
		STF		diff
. mul
		LDF     a
		MULF	b
		STF		prod
. div
		LDF     a
		DIVF	b
		STF		quot
. comp
		LDF     a
		COMPF	b

halt	J		halt

a		RESB	5
b		RESB	5
sum		RESF	1
diff	RESF	1
prod	RESF	1
quot	RESF	1
        END     first

