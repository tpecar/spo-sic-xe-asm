package asm.code;

/**
 * Predstavlja vrstico, ki vsebuje le komentar.
 * 
 * Mnemonik je s tem null.
 *
 * @author jure
 */
public class Comment extends Node {

    public Comment(String comment) {
        super(null);
        setComment(comment);
    }

    @Override
    public String toString() {
        return comment;
    }
    
    @Override
    public int length() {
        return 0;
    }

    @Override
    public void resolve(Code code) {
        // nima operanda
    }

    @Override
    public void emitCode(Code code, byte[] data, int pos) {
        // ne tvori kode
    }
}
