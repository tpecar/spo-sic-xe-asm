/*
 * TP, 2017.
 */
package asm.code;

import asm.mnemonics.Mnemonic;

/**
 * Instrukcija formata 3.
 * 
 * Ta bodisi
 *  - nima operandov (RSUB)
 *  - ima 1 operand/simbol, kateremu pa lahko sledi se indikator za
 *    indeksno naslavljanje (ki si ga shranimo kot zastavico, saj v 1. stopnji
 *    se ne poznamo vrednosti EQU stavkov/label, posledicno ne moremo
 *    izracunati naslova)
 * Instrukcija je sicer lahko razsirjena, vendar se to razresi ze na nivoju
 * map-a ukazov, saj se mnemoniki za format 4 shranjujejo loceno.
 * 
 * Za nix zastavice si hranimo stanje (te dolocimo ze v prvem prehodu, tekom
 * interpretacije operanda mnemonika - to pocnejo MnemonicF3X razredi)
 * 
 * @author tpecar
 */
public class InstructionF3 extends InstructionF34 {
    /* operacijske kode so opisane znotraj locenega razreda Opcode */
    /* KONSTRUKTORJI */
    public InstructionF3(
            Mnemonic mnemonic, int value,
            TargetAddressingMode targetAddressingMode, 
            boolean usingIndexedAddressing
    )
    {
        super(mnemonic, value, targetAddressingMode, usingIndexedAddressing);
    }
    public InstructionF3(
            Mnemonic mnemonic, String symbol,
            TargetAddressingMode targetAddressingMode, 
            boolean usingIndexedAddressing
    )
    {
        super(mnemonic, symbol, targetAddressingMode, usingIndexedAddressing);
    }
    
    @Override
    public int length() {
        return 3;
    }

    @Override
    public TargetAddressGeneration getAddressGeneration(Code code) throws SemanticError {
        // pc relativno
        if((value > code.loc ? value - code.loc : code.loc - value) < Code.MAX_SICXE_SIG_OPERAND)
            return TargetAddressGeneration.PC_RELATIVE;
        
        // bazno relativno (to je mozno le v primeru, ce je naslov baznega
        // registra pred ciljnim naslovom, saj se tu uporablja nepredznacen
        // odmik)
        if(code.baseAddress>=0 && code.baseAddress < value && value - code.baseAddress < Code.MAX_SICXE_OPERAND)
            return TargetAddressGeneration.BASE_RELATIVE;
        
        // ce je naslov dovolj majhen, da je mozno direktno naslavljanje,
        // uporabimo tega (zahteva modifikacijske zapise!)
        if(value < Code.MAX_SICXE_OPERAND)
            return TargetAddressGeneration.DIRECT;
        
        // nobeno naslavljanje ni mozno, vrzemo napako
        throw new SemanticError("Cannot generate address 0x%X");
    }
    
    @Override
    public void emitCode(Code code, byte[] data, int pos) throws SemanticError {
        // opcode imamo ze shiftano za 2 bita navzgor
        data[pos] = (byte)(super.mnemonic.opcode | targetAddressingMode.ni);
        // x bit dolocimo glede na zastavico, e bit pa glede na format instrukcije
        
        // bp bita je potrebno dolociti glede na vrednost operanda ter
        // trenutno vrednost lokacijskega stevca
        TargetAddressGeneration tag = getAddressGeneration(code);
        int operand = generateTargetAddress(code, tag);
        // xbpe biti + najvisji 4 biti operanda
        data[pos+1] = (byte)((super.isUsingIdexedAddressing() ? 1<<7 : 0) |
                             tag.bp << 5 |
                             ((operand >>> 8) & 0x0F));
        // spodnjih 8 bitov operanda
        data[pos+2] = (byte)operand;
    }
}
