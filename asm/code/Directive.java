/*
 * TP, 2017.
 */
package asm.code;

import asm.mnemonics.Mnemonic;

/**
 * (Ne-storage) direktive prevajalniku.
 * 
 * Ta oblika direktiv se zagotovo ne prevede v kodo, hkrati pa nikoli ne
 * morejo imeti labele.
 * 
 * Lahko pa ima
 *  - 0,1 argument (v primeru ce nima argumentov, se predpostavi stevilski
 *      argument 0 - to sem razbral iz predloge)
 *  - komentar, ki sledi argumentu - ta komentar se praviloma nastavi znotraj
 *    parseInstruction (lahko je dolzine 0, tj. da ni komentarja - tega
 *    zgenerira substring znotraj Lexerja)
 * 
 * value/symbol morata biti public (in posledicno final), da lahko direktno
 * dostopamo do njiju brez getter metod
 * value se spreminja znotraj resolve, posledicno bi potrebovali getter
 * 
 * Trenutna razlicica zaradi poenostavitve ne dopusca, da bi bil operand EQU
 * mnemonika simbol (razen *)!
 * (zato, da lahko predposatvimo direktno razresevanje)
 *  Simbol * je poseben primer pri EQU, ki zahteva obicajen nacin razresitve
 *  labele (tj. labeli priredi trenutno vrednost lokacijskega stevca)
 * 
 * @author tpecar
 */
public class Directive extends Node {
    /* operand, ki je direktna vrednost */
    private int value;
    /* operand, ki je simbol (ki ga bo potrebno se razresiti v vrednost) */
    private final String symbol;
    
    /* OPERACIJSKE KODE */
    /* v primeru direktiv se nobena ne preslika v veljavno operacijsko kodo */
    public static final int NOBASE = -100;
    public static final int LTORG  = -101;
    public static final int START  = -102;
    public static final int END    = -103;
    public static final int BASE   = -104;
    public static final int EQU    = -105;
    public static final int ORG    = -106;
    
    /* KONSTRUKTORJI */
    /* direktiva z vrednostjo kot operand */
    public Directive(Mnemonic mnemonic, int value) {
        super(mnemonic);
        this.symbol = null;
        this.value = value;
    }
    
    /* direktiva s simbolom kot operand */
    public Directive(Mnemonic mnemonic, String symbol) {
        super(mnemonic);
        this.symbol = symbol;
        this.value = 0;
    }
    
    /* getterji */
    public int getValue() {
        return value;
    }
    public String getSymbol() {
        return symbol;
    }
    
    /* povozimo enter metodo, da obravnavamo primer BASE direktive - pri tem
       ta nastavi vrednost baznemu registru tudi, ko se value znotraj direktive
       ni veljaven - to dovolimo, glavno je da je v casu emitCode ta razresena
    */
    @Override
    public void enter(Code code) {
        // klicamo privzeto metodo, da nastavi lokacijski stevec
        super.enter(code);
        // v primeru base, nobase vrednost baznega registra
        if(mnemonic.opcode == BASE)
            code.baseAddress = code.loc;
        if(mnemonic.opcode == NOBASE)
            code.baseAddress = -1;
        // ce ni nobena izmed direktiv, se ohranja trenutna predvidena vrednost
        // baznega registra
        
        // nastavitev imena programa
        if(code.name==null && mnemonic.opcode == START)
            code.name = super.label;
    }
    
    @Override
    public int length() {
        return 0;
    }
    
    /* povozimo privzeto implementacijo znotraj Node, da lahko posebej
       obravnavamo EQU */
    @Override
    public void activate(Code code) throws SemanticError {
        // v primeru EQU direktive dodamo operand direktive v simbolno tabelo
        if(super.mnemonic.opcode == EQU) {
            if(label==null)
                throw new SemanticError("Expected label for an EQU statement.");
            if(symbol!=null && symbol.length()>0) {
                // dovolimo le simbol *, ki zahteva privzeto obravnavo labele
                if(symbol.equals("*"))
                    super.activate(code);
                else
                    throw new SemanticError(
                            String.format("Only single level symbol resolution supported (attempted to assign symbol [%s] to symbol [%s])",
                                    symbol, label));
            }
            else // simbol je null, operand je torej vrednost (konstanta)
                code.symbols.put(label, value);
        }
        // druge direktive obdelamo kot ostale mnemonike, tj. morebitna labela
        // se razredi v vrednost PC stevca (seveda direktive se ne prevedejo
        // v kodo, posledicno kazejo na naslov prvega prevedljivega ukaza, ki
        // pride za njimi)
        else
            super.activate(code);
    }
    
    @Override
    public void resolve(Code code) throws SemanticError {
        // v primeru da ni EQU (za kar poskrbimo ze znotraj activate, ki se naj
        // bi izvedel pred resolve), poskusamo razresiti simbol, ce je ta sploh
        // prisoten
        if(symbol != null) {
            if(code.symbols.containsKey(symbol))
                value = code.symbols.get(symbol);
            else
                throw new SemanticError(String.format("Symbol [%s] undefined", symbol));
        }
        // ce ob tem gre za START direktivo, se dobljena vrednost v
        // naslednjem prehodu uporabi kot zacetno vrednost lokacijskega stevca
        if(mnemonic.opcode == START)
            code.startAddr = value;
        // podobno velja za end direktivo
        if(mnemonic.opcode == END)
            code.loadAddr = value;
    }

    @Override
    public void emitCode(Code code, byte[] data, int pos) {
        // ne tvori kode
    }
}
