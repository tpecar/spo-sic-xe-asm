/*
 * TP, 2017.
 */
package asm.code;

/**
 * Doloca, kako se naj izracunani Uporabni Naslov (UN) uporabi.
 * 
 * To se tekom prevedbe pretvori v vrednosti ni bitov.
 * 
 * bp bita se dolocita sele v 2. stopnji, ko poznamo naslove, torej sele ko
 * generiramo kodo
 * 
 * @author tpecar
 */
public enum TargetAddressingMode {
    SIC_SIMPLE(0, ""),  // enostavno naslavljanje (edini nacin pri SIC formatu):
                    //      vrednost <=  *UN,  *UN <= vrednost
    IMMEDIATE(1, "#"),   // takojsnje naslavljanje
                    //      vrednost <=   UN, (obratno ni dovoljeno)
    INDIRECT(2, "@"),    // posredno naslavljanje
                    //      vrednost <= **UN, **UN <= vrednost
    SIMPLE(3, ""),       // enostavno naslavljanje v SIC/XE nacinu
                    //      vrednost <=  *UN,  *UN <= vrednost
    ;
    
    public final int ni;
    public final String identifier;
    
    TargetAddressingMode(int ni, String identifier) {
        this.ni = ni;
        this.identifier = identifier;
    }
    
    @Override public String toString() {
        return identifier;
    }
}
