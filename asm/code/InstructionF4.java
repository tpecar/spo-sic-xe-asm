/*
 * TP, 2017.
 */
package asm.code;

import asm.mnemonics.Mnemonic;

/**
 * Instrukcija formata 4.
 * 
 * Ta ima vedno 1 operand/simbol.
 * Dolocitev, da je instrukcija razresena, se doloci  znotraj parseOperand, oz.
 * pri inicializaciji mnemonikov (tu smo lahko prepricani da dobimo mnemonik za
 * format 4).
 * 
 * Za nix zastavice dobimo tekom interpretacija znotraj MnemonicF4X razredov.
 * 
 * @author tpecar
 */
public class InstructionF4 extends InstructionF34 {
    /* operacijske kode so opisane znotraj locenega razreda Opcode */
    /* KONSTRUKTORJI */
    public InstructionF4(
            Mnemonic mnemonic, int value,
            TargetAddressingMode targetAddressingMode, 
            boolean usingIndexedAddressing
    )
    {
        super(mnemonic, value, targetAddressingMode, usingIndexedAddressing);
    }
    public InstructionF4(
            Mnemonic mnemonic, String symbol,
            TargetAddressingMode targetAddressingMode, 
            boolean usingIndexedAddressing
    )
    {
        super(mnemonic, symbol, targetAddressingMode, usingIndexedAddressing);
    }
    
    @Override
    public int length() {
        return 4;
    }
    
    @Override
    public TargetAddressGeneration getAddressGeneration(Code code) throws SemanticError {
        // pc relativno
        if((value > code.loc ? value - code.loc : code.loc - value) < Code.MAX_SICXE_SIG_EXT_OPERAND)
            return TargetAddressGeneration.PC_RELATIVE;
        
        // bazno relativno (to je mozno le v primeru, ce je naslov baznega
        // registra pred ciljnim naslovom, saj se tu uporablja nepredznacen
        // odmik)
        // bazno relativno naslavljanje naj bi po dogovoru bilo pri formatu 4 nepodprto
        //if(code.baseAddress>=0 && code.baseAddress < value && value - code.baseAddress < Code.MAX_SICXE_EXT_OPERAND)
        //    return TargetAddressGeneration.BASE_RELATIVE;
        
        // ce je naslov dovolj majhen, da je mozno direktno naslavljanje,
        // uporabimo tega (zahteva modifikacijske zapise!)
        if(value < Code.MAX_SICXE_EXT_OPERAND)
            return TargetAddressGeneration.DIRECT;
        
        // nobeno naslavljanje ni mozno, vrzemo napako
        throw new SemanticError("Cannot generate address 0x%X");
    }
    
    @Override
    public void emitCode(Code code, byte[] data, int pos) throws SemanticError {
        // opcode imamo ze shiftano za 2 bita navzgor
        data[pos] = (byte)(super.mnemonic.opcode | targetAddressingMode.ni);
        // x bit dolocimo glede na zastavico, e bit pa glede na format instrukcije
        
        // bp bita je potrebno dolociti glede na vrednost operanda ter
        // trenutno vrednost lokacijskega stevca
        TargetAddressGeneration tag = getAddressGeneration(code);
        int operand = generateTargetAddress(code, tag);
        // xbpe biti + najvisji 4 biti operanda
        data[pos+1] = (byte)((super.isUsingIdexedAddressing() ? 1<<7 : 0) |
                             tag.bp << 5 |
                             1 << 4 | // extended bit
                             ((operand >>> 16) & 0x0F));
        // vmesnih 8 bitov
        data[pos+2] = (byte)(operand >>> 8);
        // spodnjih 8 bitov
        data[pos+3] = (byte)(operand);
    }
}
