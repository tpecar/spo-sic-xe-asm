package asm.code;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Podporni razred za predmet Sistemska programska oprema.
 *
 * @author jure
 */
public class Code {
    /* KONSTANTE ------------------------------------------------------------ */
    /* registri */
    public static final String REGISTERS = "AXLBSTF";
    /* operand nerazsirjenega SIC/XE ukaza */
    public static final int MAX_SICXE_OPERAND = 1<<12;
    public static final int MAX_SICXE_SIG_OPERAND = 1<<11;
    public static final int MIN_SICXE_SIG_OPERAND = -(1<<11);
    /* operand SIC/XE registrskega ukaza */
    public static final int MAX_REG_OPERAND = 1<<4;
    /* negativnega shifta ne podpiramo */
    /* operand razsirjenega SIX/XE ukaza */
    public static final int MAX_SICXE_EXT_OPERAND = 1<<20;
    public static final int MAX_SICXE_SIG_EXT_OPERAND = 1<<19;
    public static final int MIN_SICXE_SIG_EXT_OPERAND = -(1<<19);
    
    public static final int MAX_WORD = 1<<24; /* beseda je 24-bit */
    public static final int MAX_ADDR = 1<<20; /* naslovni prostor je 20-bit */
    
    /* SPREMENLJIVKE (ki se za vsak prehod ponastavijo) --------------------- */
    /* trenutni lokacijski stevec (pridobi trenutno vrednost naslednjega
       lokacijskega stevca znotraj enter(...) metode) */
    public int loc;
    
    /* naslednji lokacijski stevec (se nastavi znotraj enter(...) metode) */
    public int nextLoc;

    /* SPREMENLJIVKE (ki se skozi prehode ohranjajo) ------------------------ */
    List<Node> program;
    Map<String, Integer> symbols;
    
    /* ime programa - dolocen ob prvem obhodu, ko se doloci labela START
       direktive - ce direktive ni, se ohrani privzeta vrednost */
    public String name = null;
    
    /* zacetni naslov lokacijskega stevca - ga moramo upostevati ob generiranju
       kode, posledico s tem pa pri tvorbi modifikacijskih zapisov */
    public int startAddr = 0; // do START direktive znotraj resolve prehoda 
                              // je 0 - ce te direktive ni, se uporabi privzeto
    
    /* nalagalni naslov programa - ga upostevamo ob generiranju objektne kode */
    public int loadAddr = 0;
    
    /* predvidena vrednost baznega registra - ce je <0, pomeni, da se ne
       uporablja */
    public int baseAddress;
    
    /* KONSTRUKTOR */
    public Code() {
        program = new LinkedList<>();
        /*
        Pri simbolih velja naslednje:
            - ce se mi omejimo na simbol = stevilska konstanta, ter se
              zadovoljimo da EQU prav tako le prireja konstanto labeli
            - prav tako se odrecemo kakrsnemu koli gnezdenju, tj.
              simbol EQU simbol ni veljavna operacija
        potem
            - razresimo vse simbole tako, da ce
                - labeli sledi EQU izraz, potem se labela bodisi direktno
                  razresi (simbolu sledi vrednost, se doda v symbols)
                  ze v 1. prehodu
                - labeli sledi mnemonik, potem se izracunan locctr da v symbols
                  ze v 1. prehodu
            - vsi desni naslovi (simboli, ki nastopajo kot operandi) se s tem
              zagotovo dajo razresiti v 2. prehodu ob generiranju kode - ce
              kaksen ni prisoten, to predstavlja napako (ker gre za simbol, ki
              ne sledi zgornjim dvem predpostavkam ali pa ta sploh ni bil
              nikoli definiran)
        */
        symbols = new HashMap<>();
    }
    
    /**
     * Doda novo vrstico v kodo.
     * 
     * Pri cemer je to vrstica z mnemonikom, direktivo
     * ali pa le s komentarjem - vsi so izpeljani iz Node razreda.
     * @param node
     */
    public void append(Node node) {
        program.add(node);
    }
    
    /**
     * Tvori pretty-print.
     * 
     * Tu sicer gre za podoben postopek kot listing, le da se tu ne naredi
     * nobene prevedbe kode, izracunov lokacijskih stevcev, razresevanja
     * simbolov ipd.
     * 
     * Tu gre dejansko le za izpis [label] [mnemonic] [mnemonic operand] [comment]
     */
    public void print() {
        for(Node node : program) {
            // labela
            System.out.format("%-10s ", node.label != null ? node.label : "");
            // ukaz/direktiva
            if(node.mnemonic != null) {
                String operand = node.mnemonic.operandToString(node);
                System.out.format("%-20s ", operand!=null && operand.length()>0 ?
                        node.mnemonic.name+" "+operand :
                        node.mnemonic.name);
            }
            // komentar
            if(node.comment != null && node.comment.length()>0)
                System.out.println(node.comment);
            else
                System.out.println();
        }
    }
    
    /* "VISITOR" METODE ------------------------------------------------------*/
    /* izvedejo sprehod cez celoten program, ter izvedejo posamezno stopnjo
       prevajanja */
    
    /* pomozne metode */
    /**
     * Pripravi Code objekt za zacetek sprehoda po programu.
     */
    public void begin() {
        // vrednost loc je sicer nepomembna, ker se op prvem enter() znotraj
        // Node postavi na nextLoc, vendar raje ne ohranjamo prejsnjih stanj
        loc = nextLoc = startAddr;
        baseAddress = -1;
        
        // startAddr se ohrani od prejsnjega cikla
    }
    
    /**
     * Izvede se misc operacije po koncu sprehoda.
     * 
     * Namenjeno razsiritvam.
     */
    public void end() {
        
    }
    
    /**
     * Pridobi vse simbole.
     */
    public void activate() throws SemanticError {
        begin();
        for(Node node : program) {
            node.enter(this);
            node.activate(this);
            node.leave(this);
        }
        end();
    }
    
    /**
     * Razresi vrednosti vseh simbolov, ki nastopajo ko operandi.
     */
    public void resolve() throws SemanticError {
        begin();
        for(Node node : program) {
            node.enter(this);
            node.resolve(this);
            node.leave(this);
        }
        end();
    }
    
    /**
     * Zgenerira kodo.
     * 
     * Predpostavimo, da sta activate ter resolve ze bila klicana.
     * 
     * Tu bi prav tako morali obravnavati modifikacijske zapise.
     * (bi imeli strukturo za vodenje lokacij, pri katerih je bil mnemonik
     *  formata 3/4, hkrati pa je bilo zahtevano direktno naslavljanje)
     * 
     * @return
     * @throws SemanticError 
     */
    public byte[] emitCode() throws SemanticError {
        // izracunana dolzina velja le, ce klicemo emitCode direktno za
        // kaksnim prehodom
        byte buffer[] = new byte[nextLoc-startAddr];
        
        begin();
        for(Node node : program) {
            node.enter(this);
            node.emitCode(this, buffer, loc-startAddr);
            node.leave(this);
        }
        end();
        
        return buffer;
    }
    
    /**
     * Zgenerira kodo, primerno za zapis v objektno datoteko.
     * 
     * Uporabi emitCode()
     * 
     * @throws SemanticError 
     */
    public final int LINE_BYTES = 30; // koliko byte-ov na vrstico v
                                      // objektni datoteki
    
    public void emitText(StringBuilder sb) throws SemanticError {
        byte buffer[] = emitCode();
        
        sb.append(String.format("H%6s%06X%06X\n", name, startAddr, buffer.length));
        for(int progIdx=0; progIdx < buffer.length;) {
            sb.append(String.format("T%06X%02X",
                    progIdx+startAddr,
                    (buffer.length-progIdx < LINE_BYTES ? buffer.length-progIdx : LINE_BYTES)
                    ));
            for(int lineIdx=0; progIdx < buffer.length && lineIdx < 30; progIdx++, lineIdx++) {
                sb.append(String.format("%02X", buffer[progIdx]));
            }
            sb.append('\n');
        }
        sb.append(String.format("E%06X\n", loadAddr));
    }
    
    /**
     * Tvori listing datoteko.
     * Predpostavi, da sta activate ter resolve ze bila klicana.
     */
    public void generateListing(StringBuilder sb) throws SemanticError {
        begin();
        for(Node node : program) {
            node.enter(this);
            // lokacijski stevec - izpisemo le ce je drugacen prejsnjemu
            sb.append(String.format("%5s  ", loc != nextLoc ? String.format("%05X", loc) : ""));
            // generirana koda
            StringBuilder lineCode = new StringBuilder();
            node.emitText(this, lineCode);
            sb.append(String.format("%-10s ",
                    lineCode.length()<10 ?
                            lineCode.toString() :
                            lineCode.toString().substring(0, 7)+"..."));
            
            // labela
            sb.append(String.format("%-10s ", node.label != null ? node.label : ""));
            // ukaz/direktiva
            if(node.mnemonic != null) {
                String operand = node.mnemonic.operandToString(node);
                sb.append(String.format("%-20s ", operand!=null && operand.length()>0 ?
                        node.mnemonic.name+" "+operand :
                        node.mnemonic.name));
            }
            // komentar
            if(node.comment != null && node.comment.length()>0)
                sb.append(node.comment);
            sb.append('\n');
            node.leave(this);
        }
        end();
    }
    public void dumpSymbols(StringBuilder sb) {
        sb.append("Symbols\n");
        ArrayList<Map.Entry<String, Integer>> symbolList = new ArrayList<>(symbols.entrySet());
        
        Collections.sort(symbolList, new Comparator<Map.Entry<String, Integer>>(){
            @Override
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return a.getKey().compareTo(b.getKey());
            }
        });
        sb.append(String.format("   %-10s %6s %6s\n", "name", "hex", "dec"));
        for(Map.Entry<String, Integer> symbol : symbolList)
            sb.append(String.format("   %-10s %2$06X %2$6d\n", symbol.getKey(), symbol.getValue()));
    }
}
