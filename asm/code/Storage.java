/*
 * TP, 2017.
 */
package asm.code;

import asm.mnemonics.Mnemonic;

/**
 * Storage direktive prevajalniku.
 * 
 * Ta oblika direktiv se prevede bodisi v
 *  - binarni niz
 *  - prazen (nealociran) prostor
 * ki se vrine med kodo.
 * 
 * V obeh primerih je potrebno hraniti dolzino rezerviranega prostora, le da
 * v primeru dejanske alokacije se hranimo tabelo na byte
 * (ta je pri rezervaciji null)
 * 
 * @author tpecar
 */
public class Storage extends Node {
    /* stevilo rezerverviranih byte-ov */
    public final int allocatedSize;
    
    /* vrednosti, ki jih shranimo na alocirano obmocje - direktnega dostopa
       ne moremo omogociti, saj ceprav bi referenca bila konstanta, bi lahko
       spreminjali vsebino */
    private final byte[] storedBytes;
    
    /* OPERACIJSKE KODE */
    /* se ne preslikajo v ukaz */
    
    public static final int RESB = -200;
    public static final int RESW = -201;
    public static final int BYTE = -202;
    public static final int WORD = -203;
    
    /* KONSTRUKTORJI */
    public Storage(Mnemonic mnemonic, int allocatedSize) {
        super(mnemonic);
        this.allocatedSize = allocatedSize;
        this.storedBytes = null;
    }
    
    /* tabelo byte-ov poda specificna storage direktiva */
    public Storage(Mnemonic mnemonic, int allocatedSize, byte[] storedBytes) throws SemanticError {
        super(mnemonic);
        if(allocatedSize < storedBytes.length)
            throw new SemanticError(
                    String.format("Allocated less bytes (%d) than provided (%d)",
                            allocatedSize, storedBytes.length)
            );
        if(allocatedSize > storedBytes.length)
            System.out.format("WARN: Allocated more bytes (%d) than provided (%d)\n",
                    allocatedSize, storedBytes.length);
        this.allocatedSize = allocatedSize;
        this.storedBytes = storedBytes;
    }
    
    @Override
    public int length() {
        return allocatedSize;
    }
    
    /* pomozna metoda za izpis operanda */
    public String dataToString() {
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<storedBytes.length; i++)
            sb.append(String.format("%02X", storedBytes[i]));
        return sb.toString();
    }
    /* metode za posredno pridobivanje vrednosti */
    public int getWord() {
        return (((int)storedBytes[0]) & 0xFF)<<16 |
               (((int)storedBytes[1]) & 0xFF)<<8  |
               (((int)storedBytes[2]) & 0xFF);
    }

    @Override
    public void resolve(Code code) throws SemanticError {
        // ne dovolimo simbolov za kolicino podatkov
        // (bi mocno otezilo racunanje lokacijskega stevca)
    }

    @Override
    public void emitCode(Code code, byte[] data, int pos) throws SemanticError {
        // tvorimo kodo le v primeru inicializacije
        if(storedBytes != null) {
            System.arraycopy(storedBytes, 0, data, pos, storedBytes.length);
        }
    }
}
