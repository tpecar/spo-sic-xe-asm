/*
 * TP, 2017.
 */
package asm.code;

import asm.mnemonics.Mnemonic;

/**
 * Instrukcija formata 2 (1-2 operanda).
 * 
 * Operandi so tipicno registri (torej gre za stevilske konstante, ki jih
 * preoblikujemo preko parseRegister),
 * 
 * v primeru SHIFTL, SHIFTR pa je lahko drugi operand stevilska vrednost
 * (ki pove za koliko mest se izvede zamik) - SYSS pri teh ukazih namrec
 * opis drugi operand kot r2, ne pa (r2) - sele slednje bi namrec pomenilo
 * vsebina registra.
 * 
 * Za format vhoda skbijo MnemonicF2XX razredi, tako da lahko v primeru klica
 * konstruktorja tu predvidimo, da bomo vedno dobili stevilski vrednosti.
 * 
 * (tudi v primeru, ce ima ukaz le en operand, se za drugega privzame vrednost 0)
 * 
 * Simbolov pri F2 ukazih ne dopuscamo.
 * 
 * @author tpecar
 */
public class InstructionF2 extends Node {
    /* prvi register */
    private final int r1;
    
    /* drugi register, oz. stevilo mest zamika */
    private final int r2;
    
    /* operacijske kode so opisane znotraj locenega razreda Opcode */
    /* KONSTRUKTORJI */
    public InstructionF2(Mnemonic mnemonic, int r1, int r2) {
        super(mnemonic);
        this.r1 = r1;
        this.r2 = r2;
    }
    
    /* getterji */
    public int getR1() {
        return r1;
    }
    public int getR2() {
        return r2;
    }
    
    @Override
    public int length() {
        return 2;
    }
    
    @Override
    public void resolve(Code code) {
        // operandi ne morejo biti simboli
    }
    
    @Override
    public void emitCode(Code code, byte[] data, int pos) {
        // dana instrukcija je neodvisna od lokacije, posledicno code tu ni
        // potreben
        data[pos] = (byte)super.mnemonic.opcode;
        data[pos+1] = (byte)(r1<<4 | r2);
    }
}
