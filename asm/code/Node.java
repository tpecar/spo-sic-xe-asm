package asm.code;

import asm.mnemonics.Mnemonic;

/**
 * Node.
 * 
 * Posamezna vrstica programa, hkrati pa tudi najmanjsa prevajalna enota.
 * Ta 
 * 
 * @author jure
 */
public abstract class Node {

    protected String label;
    protected Mnemonic mnemonic;
    protected String comment;

    public Node(Mnemonic mnemonic) {
        this.mnemonic = mnemonic;
    }
    /* labela ter komentar se nastavita znotraj Parser -> parseInstruction */

    public String getLabel() {
        return label == null ? "" : label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Return comment as a string.
     */
    public String getComment() {
        return comment == null ? "" : comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Return string representation of the node. Label and comment are not
     * included.
     */
    @Override
    public String toString() {
        return mnemonic.toString() + " " + operandToString();
    }

    /**
     * Pridobi operand iz mnemonika.
     * 
     * Vsak tip mnemonika hrani svoje operande (saj ti so lahko razlicni, kljub
     * istemu formatu).
     * 
     * @return 
     */
    public String operandToString() {
        return mnemonic.operandToString(this);
    }
    
    /**
     * Vrne dolzino (v bajtih) prevedljivega odseka vrstice.
     * 
     * Odvisno od podrazreda, ki ga implementira:
     *  - komentar ima recimo dolzino 0,
     *  - dolzina ukazov je odvisna od njihovega formata
     *  - storage direktive (ki so tretirane loceno od preostalih direktiv)
     *    imajo dolzino svojih podatkov
     *  - preostale direktive imajo dolzino 0 (se ne preslikajo v kodo)
     * 
     * @return 
     */
    public abstract int length();
    
    /**
     * Pripravi stanje za naslednji odsek kode.
     * Tipicno le nastavljanje lokacijskega stevca.
     * @param code trenutno stanje asemblerja
     */
    public void enter(Code code) {
        code.loc = code.nextLoc;
        code.nextLoc += length();
    }
    /**
     * Popravki stanja glede na mnemonik po njegovi interpretaciji.
     * Tipicno nic, le v primeru ORG direktive (povozi metodo).
     * @param code trenutno stanje asemblerja
     */
    public void leave(Code code) {
        
    }
    
    /**
     * Definira simbol ki ga podajajo labele ukazov.
     * 
     * Te lahko razresimo tekom 1. prehoda, saj je naslov ukaza znan preko
     * lokacijskega stevca (v Code objektu).
     * 
     * Pri tem preverimo, da ne dodajamo ze definiranega simbola.
     * 
     * To povozimo v primeru direktive, zato da pravilno obravnavamo primer z
     * EQU.
     * 
     * @param code 
     */
    public void activate(Code code) throws SemanticError {
        // ce labele ni, ni nic za razresiti (dana vrstica ne tvori novega
        // simbola)
        if(label!=null) {
            if(code.symbols.containsKey(label))
                throw new SemanticError(
                        String.format("Symbol %s already present (tried to replace value %d with %d)",
                                label, code.symbols.get(label), code.loc));
            // ce podan simbol se ne obstaja, dodamo naso labelo kot
            // razresen simbol (trenutna vrednost PC stevca, na kateri se nahaja
            // dan Node)
            code.symbols.put(label, code.loc);
        }
    }
    
    /**
     * Razresi morebitne simbole, ki jih ima operand.
     * 
     * To razresujemo tekom 2. prehoda, oz. takrat ko smo vse labele ter EQU
     * izraze ze razresili ter shranili v simbolno tabelo.
     * 
     * V primeru operandov gremo razresevati le v primeru, ce symbol
     * spremenljivka ni enaka null.
     * Ob razresitvi nastavimo value, symbol pa bi lahko nastavili na null,
     * vendar zavoljo pravilnega listinga tega ne zelimo.
     * 
     * Veckratnemu razresevanju brez eksplicitne indikacije za to se izognemo ob 
     * predpostavki, da se za vsak Node klice resolve le enkrat (v primeru 
     * globine 1 za simbole je to mozno, za razresevanje bi bilo potrebno 
     * resolve izvesti veckrat, oz. tolikokrat dokler ne razresimo vseh simbolov 
     * - tu bi bilo potrebno, da si hranimo status razresenosti s kaksno boolean
     *   spremenljivko)
     * 
     * @param code stanje prevajanja
     */
    public abstract void resolve(Code code) throws SemanticError;
    
    /**
     * Zapise kodo (ce jo generira) v tabelo na podani poziciji.
     * 
     * Dolzino tabele dolocimo ob prvem prehodu na podlagi koncne vrednosti
     * lokacijskega stevca.
     * 
     * @param code trenutno stanje prevajanja (lokacijski stevec, potreben za
     *             dolocitev bp bitov pri formatu 3/4)
     * @param data tabela, v katero zapisujemo
     * @param pos zacetna pozicija, od katere (vkljucno) zapisujemo
     */
    public abstract void emitCode(Code code, byte[] data, int pos) throws SemanticError;
    
    /**
     * Zapise generirano kodo v obliki niza.
     * @param code trenutno stanje prevajanja (potreben za emitCode)
     * @param buf znakovni buffer, na koncu katerega pripnemo niz
     */
    public void emitText(Code code, StringBuilder buf) throws SemanticError {
        byte[] buffer = new byte[length()];
        
        emitCode(code, buffer, 0);
        for(int i=0; i<buffer.length; i++)
            buf.append(String.format("%02X",buffer[i]));
    }
}
