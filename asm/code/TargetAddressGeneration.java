/*
 * TP, 2017.
 */
package asm.code;

/**
 * Doloca, na podlagi cesa se doloci Uporabni Naslov pri formatu 3/4.
 * 
 * @author tpecar
 */
public enum TargetAddressGeneration {
    DIRECT(0),          // neposredno naslavljanje (edino pri SIC)
                        //      UN <= operand
    PC_RELATIVE(1),     // pc relativno naslavljanje
                        //      UN <= operand + PC
    BASE_RELATIVE(2),   // bazno naslavljanje
                        //      UN <= operand + B
    ;
    
    public final int bp;
    
    TargetAddressGeneration(int bp) {
        this.bp = bp;
    }
}
