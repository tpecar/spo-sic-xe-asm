/*
 * TP, 2017.
 */
package asm.code;

import asm.mnemonics.Mnemonic;

/**
 * Instrukcija formata 1 (brez operandov).
 * @author tpecar
 */
public class InstructionF1 extends Node {
    public InstructionF1(Mnemonic mnemonic) {
        super(mnemonic);
    }
    
    @Override
    public int length() {
        return 1;
    }
    
    @Override
    public void resolve(Code code) {
        // nima operanda
    }

    @Override
    public void emitCode(Code code, byte[] data, int pos) {
        // vsaj glede na trenutni nabor ukazov lahko privzamemo, da je
        // opcode vedno znotraj 1ega byte-a
        data[pos] = (byte)super.mnemonic.opcode;
    }
}
