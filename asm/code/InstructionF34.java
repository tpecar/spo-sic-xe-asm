/*
 * TP, 2017.
 */
package asm.code;

import asm.mnemonics.Mnemonic;

/**
 * Instrukcija formata 3.
 * 
 * Tako instrukcija formata 3 kot 4 morata hraniti 1 vrednost/simbol,
 * nacin uporabe UN ter morebitna moznost uporabe indeksnega naslavljanja je
 * prisotna pri obeh formatih.
 * (v prvotnem SIC/XE procesorju so sicer naslavljanja/uporaba indeksnega
 *  naslavljanja pri formatu 4 sicer omejena, vendar mi delamo na podlagi
 *  nasega simulatorja, ki to lahko sprejme)
 * 
 * @author tpecar
 */
public abstract class InstructionF34 extends Node {
    /* operand, bodisi vrednost ali simbol */
    protected int value;
    protected final String symbol;
    
    /* nacin interpretacije uporabnega naslova */
    protected final TargetAddressingMode targetAddressingMode;
    /* ali se uporablja indeksno naslavljanje */
    protected final boolean usingIndexedAddressing;
    
    /* operacijske kode so opisane znotraj locenega razreda Opcode */
    /* KONSTRUKTORJI */
    public InstructionF34(
            Mnemonic mnemonic, int value,
            TargetAddressingMode targetAddressingMode, 
            boolean usingIndexedAddressing
    )
    {
        super(mnemonic);
        this.value = value;
        this.symbol = null;
        this.targetAddressingMode = targetAddressingMode;
        this.usingIndexedAddressing = usingIndexedAddressing;
    }
    public InstructionF34(
            Mnemonic mnemonic, String symbol,
            TargetAddressingMode targetAddressingMode, 
            boolean usingIndexedAddressing
    )
    {
        super(mnemonic);
        this.value = 0;
        this.symbol = symbol;
        this.targetAddressingMode = targetAddressingMode;
        this.usingIndexedAddressing = usingIndexedAddressing;
    }
    
    /* getterji */
    public int getValue() {
        return value;
    }
    public String getSymbol() {
        return symbol;
    }
    public TargetAddressingMode getTargetAddressingMode() {
        return targetAddressingMode;
    }
    public boolean isUsingIdexedAddressing() {
        return usingIndexedAddressing;
    }
    
    @Override
    public void resolve(Code code) throws SemanticError {
        /* enaka obravnava kot pri Directive */
        if(symbol != null) {
            if(code.symbols.containsKey(symbol))
                value = code.symbols.get(symbol);
            else
                throw new SemanticError(String.format("Symbol [%s] undefined", symbol));
        }
    }
    
    /**
     * Na podlagi vrednosti (ki tipicno predstavlja naslov) ter trenutne
     * vrednosti lokacijskega stevca doloci bp bite.
     * 
     * Je implementirano posebej za format 3 ter 4 (zaradi razlicnih mej)
     * 
     * @param code trenutno stanje (lokacijskega stevca)
     * @return stanje bp bitov
     */
    public abstract TargetAddressGeneration getAddressGeneration(Code code) throws SemanticError;
    
    /**
     * Zgenerira odmik, ki se zapise v operand ukaza.
     * 
     * Skupen za format 3/4.
     * Obmocje vrednosti je sicer med formati drugacno, predpostavimo da se
     * uporabi pravi getAddressGeneration za dolocitev nacina tvorbe.
     * 
     * @param code trenutno stanje (lokacijskega stevca)
     * @param tag nacin, na podlagi katerega tvorimo
     * @return operand, ki skupaj s trenutnim stanjem tvori vrednost
     */
    public int generateTargetAddress(Code code, TargetAddressGeneration tag) {
        switch(tag) {
            case DIRECT:        return value;
            case PC_RELATIVE:   return value - code.nextLoc;
            case BASE_RELATIVE: return value - code.baseAddress;
        }
        throw new IllegalStateException("Bad address generation specifier");
    }
    
    /* Odvisno ali gre za format 3 ali 4 */
    @Override
    public abstract int length();
}
