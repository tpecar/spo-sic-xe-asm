/*
 * TP, 2017.
 */
package asm.mnemonics;

import asm.code.Code;
import asm.code.InstructionF2;
import asm.code.Node;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Mnemonik ukaza 2 z dvema registrskima operandoma.
 * 
 * @author tpecar
 */
public class MnemonicF2rr extends Mnemonic {
    
    public MnemonicF2rr(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }

    @Override
    public Node parse(Parser parser) throws SyntaxError {
        // 1. operand - register
        int r1;
        // indeks registra
        if (Character.isDigit(parser.lexer.peek())) {
            r1 = parser.parseNumber(0, Code.MAX_REG_OPERAND);
        } // simbol registra
        else if (Character.isLetter(parser.lexer.peek())) {
            r1 = parser.parseRegister();
        } // drugace: napaka
        else {
            throw new SyntaxError("register identifier", parser);
        }
        
        // preskocimo vejico
        parser.parseComma();
        
        // 2. operand - register
        int r2;
        // indeks registra
        if (Character.isDigit(parser.lexer.peek())) {
            r2 = parser.parseNumber(0, Code.MAX_REG_OPERAND);
        } // simbol registra
        else if (Character.isLetter(parser.lexer.peek())) {
            r2 = parser.parseRegister();
        } // drugace: napaka
        else {
            throw new SyntaxError("register identifier", parser);
        }
        
        return new InstructionF2(this, r1, r2);
    }
    
    @Override public String operandToString(Node instruction) {
        InstructionF2 i = (InstructionF2) instruction;
        return Code.REGISTERS.charAt(i.getR1())+", "+Code.REGISTERS.charAt(i.getR2());
    }
}
