/*
 * TP, 2017.
 */
package asm.mnemonics;

import asm.code.InstructionF3;
import asm.code.Node;
import asm.code.TargetAddressingMode;
import asm.parsing.Parser;

/**
 * Mnemonik ukaza formata 3 brez operandov.
 * @author tpecar
 */
public class MnemonicF3 extends Mnemonic {
    
    public MnemonicF3(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }
    
    @Override
    public Node parse(Parser parser) {
        /* nerabljene vrednosti nastavljene na 0 */
        return new InstructionF3(this, 0, TargetAddressingMode.SIC_SIMPLE, false);
    }
    
    @Override
    public String operandToString(Node instruction) {
        return ""; // brez operandov
    }
}
