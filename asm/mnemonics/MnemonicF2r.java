/*
 * TP, 2017.
 */
package asm.mnemonics;

import asm.code.Code;
import asm.code.InstructionF2;
import asm.code.Node;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Mnemonik ukaza formata 2 z enim registrskim operandom.
 * 
 * Registrski zapis dovolimo bodisi kot znak ali stevilo, ne dovolimo pa
 * simbola.
 * 
 * @author tpecar
 */
public class MnemonicF2r extends Mnemonic {

    public MnemonicF2r(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }

    @Override
    public Node parse(Parser parser) throws SyntaxError {
        // indeks registra
        if (Character.isDigit(parser.lexer.peek())) {
            return new InstructionF2(this, parser.parseNumber(0, Code.MAX_REG_OPERAND), 0);
        } // simbol registra
        else if (Character.isLetter(parser.lexer.peek())) {
            return new InstructionF2(this, parser.parseRegister(), 0);
        } // drugace: napaka
        else {
            throw new SyntaxError("register identifier", parser);
        }
    }
    
    @Override
    public String operandToString(Node instruction) {
        InstructionF2 i = (InstructionF2) instruction;
        return Character.toString(Code.REGISTERS.charAt(i.getR1()));
    }
}
