package asm.mnemonics;

import asm.code.Code;
import asm.code.Directive;
import asm.code.Node;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Directive with one numeric operand. Podporni razred za predmet Sistemska
 * programska oprema.
 *
 * @author jure
 */
public class MnemonicDn extends Mnemonic {

    public MnemonicDn(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }

    @Override
    public Node parse(Parser parser) throws SyntaxError {
        // stevilska vrednost
        if (Character.isDigit(parser.lexer.peek())) {
            return new Directive(this, parser.parseNumber(0, Code.MAX_SICXE_OPERAND));
        } // simbol (pri tem v primeru EQU dovolimo *)
        else if (Character.isLetter(parser.lexer.peek()) ||
                 (super.opcode == Directive.EQU && parser.lexer.peek()=='*')) {
            return new Directive(this, parser.parseSymbol());
        } // drugace: napaka
        else {
            throw new SyntaxError("symbol name", parser);
        }
    }
    /**
     * Vrne operand.
     * 
     * Pri tem se pri direktivi pricakuje, da vsebuje bodisi simbol (tj. drugo
     * labelo ali simbol EQU stavka), bodisi stevilo (ki ga pretvorimo v niz).
     * 
     * Ta metoda je namenjena generiranju listinga, ne pa za prevedbo v
     * objektno kodo.
     * 
     * @param instruction
     * @return 
     */
    @Override
    public String operandToString(Node instruction) {
        Directive i = ((Directive) instruction);
        return i.getSymbol() != null ? i.getSymbol() : Integer.toString(i.getValue());
    }
}
