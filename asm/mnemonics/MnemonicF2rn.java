/*
 * TP, 2017.
 */
package asm.mnemonics;

import asm.code.Code;
import asm.code.InstructionF2;
import asm.code.Node;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Mnemonik ukaza formata 2 s prvim registrskim operandom ter drugim stevilskim
 * operandom.
 * 
 * Za stevilski operand ne dovolimo simbolov.
 * 
 * @author tpecar
 */
public class MnemonicF2rn extends Mnemonic {
    public MnemonicF2rn(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }

    @Override
    public Node parse(Parser parser) throws SyntaxError {
        // 1. operand - register
        int r1;
        // indeks registra
        if (Character.isDigit(parser.lexer.peek())) {
            r1 = parser.parseNumber(0, Code.MAX_REG_OPERAND);
        } // simbol registra
        else if (Character.isLetter(parser.lexer.peek())) {
            r1 = parser.parseRegister();
        } // drugace: napaka
        else {
            throw new SyntaxError("register identifier", parser);
        }
        
        // preskocimo vejico
        parser.parseComma();
        
        // 2. operand - vrednost
        int r2;
        // indeks registra
        if (Character.isDigit(parser.lexer.peek())) {
            r2 = parser.parseNumber(0, Code.MAX_REG_OPERAND);
        } // drugace: napaka
        else {
            throw new SyntaxError("value", parser);
        }
        
        return new InstructionF2(this, r1, r2);
    }
    
    @Override public String operandToString(Node instruction) {
        InstructionF2 i = (InstructionF2) instruction;
        return Code.REGISTERS.charAt(i.getR1())+", "+i.getR2();
    }
}
