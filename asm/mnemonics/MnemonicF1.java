/*
 * TP, 2017.
 */
package asm.mnemonics;

import asm.code.InstructionF1;
import asm.code.Node;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Mnemonik ukaza formata 1.
 * Ta ze sam po sebi nima operandov.
 * 
 * @author tpecar
 */
public class MnemonicF1 extends Mnemonic {
    
    public MnemonicF1(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }
    
    @Override
    public Node parse(Parser parser) throws SyntaxError {
        return new InstructionF1(this);
    }
    
    @Override
    public String operandToString(Node instruction) {
        return ""; // brez operandov
    }
}
