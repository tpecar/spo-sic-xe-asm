/*
 * TP, 2017.
 */
package asm.mnemonics;

import asm.code.Code;
import asm.code.Node;
import asm.code.SemanticError;
import asm.code.Storage;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Storage direktiva z alokacijo, brez inicializacije.
 * 
 * Operand te direktive pove le, koliko byte-ov alociramo.
 * 
 * @author tpecar
 */
public class MnemonicSn extends Mnemonic {
    public MnemonicSn(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }
    
    @Override
    public Node parse(Parser parser) throws SyntaxError, SemanticError {
        int bytesPerUnit;
        switch(super.opcode) {
            case Storage.RESB: bytesPerUnit = 1; break;
            case Storage.RESW: bytesPerUnit = 3; break;
            default: throw new SemanticError("Invalid directive for allocation");
        }
        
        // stevilska vrednost
        if (Character.isDigit(parser.lexer.peek())) {
            return new Storage(this,
                    parser.parseNumber(0, Code.MAX_ADDR/bytesPerUnit)*bytesPerUnit);
        } // drugace: napaka
        else {
            throw new SyntaxError("unit count", parser);
        }
    }
    
    @Override
    public String operandToString(Node directive) {
        Storage s = (Storage) directive;
        return Integer.toString(s.allocatedSize);
    }
}
