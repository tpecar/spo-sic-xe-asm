/*
 * TP, 2017.
 */
package asm.mnemonics;

import asm.code.Code;
import asm.code.InstructionF4;
import asm.code.Node;
import asm.code.TargetAddressingMode;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Instrukcija formata 4 z enim operandom (vrednost/simbol).
 * 
 * Pri tem lahko operandu sledi dolocilo za indeksno naslavljanje.
 * V osnovi enak MnemonicF4m, s to razliko, da je meja postavljena visje.
 * 
 * Negativne vrednosti dovolimo za operand, a ni nujno, da se interpretirajo
 * pravilno (odvisno od procesroja/simulatorja).
 * 
 * Za identifikacijo, ali gre za format 4, se doloci ze znotraj parseInstruction.
 * 
 * @author tpecar
 */
public class MnemonicF4m extends Mnemonic {
    public MnemonicF4m(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }

    @Override
    public Node parse(Parser parser) throws SyntaxError {
        // prvo nacin naslavljanja
        TargetAddressingMode tam = parser.parseTA();

        // stevilska vrednost
        if (parser.lexer.peek()=='-' || Character.isDigit(parser.lexer.peek())) {
            int value = parser.parseNumber(Code.MIN_SICXE_SIG_EXT_OPERAND, Code.MAX_SICXE_EXT_OPERAND);
            boolean isIndexed = parser.parseIndexed();

            return new InstructionF4(this, value, tam, isIndexed);
        } // simbol
        else if (Character.isLetter(parser.lexer.peek())) {
            String symbol = parser.parseSymbol();
            boolean isIndexed = parser.parseIndexed();

            return new InstructionF4(this, symbol, tam, isIndexed);
        } // drugace: napaka
        else {
            throw new SyntaxError("symbol name", parser);
        }
    }
    
    @Override
    public String operandToString(Node instruction) {
        InstructionF4 i = (InstructionF4) instruction;
        return i.getTargetAddressingMode() + (i.getSymbol() != null ? i.getSymbol() : Integer.toString(i.getValue()));
    }
}
