package asm.mnemonics;

import asm.code.Directive;
import asm.code.Node;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Direktiva brez operandov.
 *
 * @author jure
 */
public class MnemonicD extends Mnemonic {

    /* konstruktor, ki se klice znotraj Parser
       Pri tem je drevo klicanja
        Asm->Parser.parse() -> Parser.parseCode() -> Parser.parseInstruction()
        -> Parser.parseMnemonic() -> Parser.get -> mnemonics.get(name)
        
        Pri cemer je mnemonics map, ki ga ime mnemonika preslika v Mnemonic
        objekt (ki je morebiti ta objekt)
    
        Nato pa se znotraj klice Parser.parseInstruction() -> mnemonic.parse(this),
        s cimer se klice parse tega mnemonika
        
        prav tako ta klice
            setLabel, ki vzame vsebino do mnemonika
            setComment, ki vzame preostalo vsebino iz vrstice (torej vsebino, ki
                jo je parse od mnemonika se pustil)
    */
    public MnemonicD(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }

    @Override
    public Node parse(Parser parser) throws SyntaxError {
        /* mnemonik brez operanda (oz. je ta privzeto 0 - v splosnem se ne smemo
           zanasati na to vrednost) */
        return new Directive(this, 0);
    }

    @Override
    public String operandToString(Node instruction) {
        return ""; // brez operandov
    }
}
