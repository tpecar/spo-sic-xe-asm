/*
 * TP, 2017.
 */
package asm.mnemonics;

/**
 * Hrani konstante za operacijske kode vseh prevedljivih ukazov.
 * 
 * @author tpecar
 */
public class Opcode {
        public static final int LDA = 0x00;     // LDA 0x00 0x00
        public static final int LDX = 0x04;     // LDX 0x04 0x01
        public static final int LDL = 0x08;     // LDL 0x08 0x02
        public static final int STA = 0x0C;     // STA 0x0C 0x03
        public static final int STX = 0x10;     // STX 0x10 0x04
        public static final int STL = 0x14;     // STL 0x14 0x05
        public static final int ADD = 0x18;     // ADD 0x18 0x06
        public static final int SUB = 0x1C;     // SUB 0x1C 0x07
        public static final int MUL = 0x20;     // MUL 0x20 0x08
        public static final int DIV = 0x24;     // DIV 0x24 0x09
        public static final int COMP = 0x28;    // COMP 0x28 0x0A
        public static final int TIX = 0x2C;     // TIX 0x2C 0x0B
        public static final int JEQ = 0x30;     // JEQ 0x30 0x0C
        public static final int JGT = 0x34;     // JGT 0x34 0x0D
        public static final int JLT = 0x38;     // JLT 0x38 0x0E
        public static final int J = 0x3C;       // J 0x3C 0x0F
        public static final int AND = 0x40;     // AND 0x40 0x10
        public static final int OR = 0x44;      // OR 0x44 0x11
        public static final int JSUB = 0x48;    // JSUB 0x48 0x12
        public static final int RSUB = 0x4C;    // RSUB 0x4C 0x13
        public static final int LDCH = 0x50;    // LDCH 0x50 0x14
        public static final int STCH = 0x54;    // STCH 0x54 0x15
        public static final int ADDF = 0x58;    // ADDF 0x58 0x16
        public static final int SUBF = 0x5C;    // SUBF 0x5C 0x17
        public static final int MULF = 0x60;    // MULF 0x60 0x18
        public static final int DIVF = 0x64;    // DIVF 0x64 0x19
        public static final int LDB = 0x68;     // LDB 0x68 0x1A
        public static final int LDS = 0x6C;     // LDS 0x6C 0x1B
        public static final int LDF = 0x70;     // LDF 0x70 0x1C
        public static final int LDT = 0x74;     // LDT 0x74 0x1D
        public static final int STB = 0x78;     // STB 0x78 0x1E
        public static final int STS = 0x7C;     // STS 0x7C 0x1F
        public static final int STF = 0x80;     // STF 0x80 0x20
        public static final int STT = 0x84;     // STT 0x84 0x21
        public static final int COMPF = 0x88;   // COMPF 0x88 0x22
        // ???? 0x8C 0x23
        public static final int ADDR = 0x90;    // ADDR 0x90 0x24
        public static final int SUBR = 0x94;    // SUBR 0x94 0x25
        public static final int MULR = 0x98;    // MULR 0x98 0x26
        public static final int DIVR = 0x9C;    // DIVR 0x9C 0x27
        public static final int COMPR = 0xA0;   // COMPR 0xA0 0x28
        public static final int SHIFTL = 0xA4;  // SHIFTL 0xA4 0x29
        public static final int SHIFTR = 0xA8;  // SHIFTR 0xA8 0x2A
        public static final int RMO = 0xAC;     // RMO 0xAC 0x2B
        public static final int SVC = 0xB0;     // SVC 0xB0 0x2C
        public static final int CLEAR = 0xB4;   // CLEAR 0xB4 0x2D
        public static final int TIXR = 0xB8;    // TIXR 0xB8 0x2E
        // ???? 0xBC 0x2F
        public static final int FLOAT = 0xC0;   // FLOAT 0xC0 0x30
        public static final int FIX = 0xC4;     // FIX 0xC4 0x31
        public static final int NORM = 0xC8;    // NORM 0xC8 0x32
        // ???? 0xCC 0x33
        public static final int LPS = 0xD0;     // LPS 0xD0 0x34
        public static final int STI = 0xD4;     // STI 0xD4 0x35
        public static final int RD = 0xD8;      // RD 0xD8 0x36
        public static final int WD = 0xDC;      // WD 0xDC 0x37
        public static final int TD = 0xE0;      // TD 0xE0 0x38
        // ???? 0xE4 0x39
        public static final int STSW = 0xE8;    // STSW 0xE8 0x3A
        public static final int SSK = 0xEC;     // SSK 0xEC 0x3B
        public static final int SIO = 0xF0;     // SIO 0xF0 0x3C
        public static final int HIO = 0xF4;     // HIO 0xF4 0x3D
        public static final int TIO = 0xF8;     // TIO 0xF8 0x3E
}
