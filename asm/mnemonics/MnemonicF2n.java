/*
 * TP, 2017.
 */
package asm.mnemonics;

import asm.code.Code;
import asm.code.InstructionF2;
import asm.code.Node;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Mnemonik ukaza formata 2 z enim stevilskim operandom (simbol/vrednost).
 * 
 * Prvi argument je vrednost, drugi argument je privzeto 0.
 * 
 * @author tpecar
 */
public class MnemonicF2n extends Mnemonic {
    
    public MnemonicF2n(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }
    
    @Override
    public Node parse(Parser parser) throws SyntaxError {
        // vrednost
        if (Character.isDigit(parser.lexer.peek())) {
            return new InstructionF2(this, parser.parseNumber(0, Code.MAX_REG_OPERAND), 0);
        } // drugace: napaka
        else {
            throw new SyntaxError("value", parser);
        }
    }
    
    @Override
    public String operandToString(Node instruction) {
        InstructionF2 i = (InstructionF2) instruction;
        return Integer.toString(i.getR1());
    }
}
