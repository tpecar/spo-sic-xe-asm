/*
 * TP, 2017.
 */
package asm.mnemonics;

import asm.code.Code;
import asm.code.InstructionF3;
import asm.code.Node;
import asm.code.TargetAddressingMode;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Mnemonik ukaza formata 3 z 1 operandom (vrednost/simbol).
 *
 * Pri tem imamo lahko za operandom se identifikator za indeksno naslavljanje (,
 * X) - za interpretacijo tega je ze poskrbljeno znotraj parserja.
 * 
 * Negativne vrednosti dovolimo za operand, a ni nujno, da se interpretirajo
 * pravilno (odvisno od procesroja/simulatorja).
 *
 * @author tpecar
 */
public class MnemonicF3m extends Mnemonic {

    public MnemonicF3m(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }

    @Override
    public Node parse(Parser parser) throws SyntaxError {
        // prvo nacin naslavljanja
        TargetAddressingMode tam = parser.parseTA();

        // stevilska vrednost
        if (parser.lexer.peek()=='-' || Character.isDigit(parser.lexer.peek())) {
            int value = parser.parseNumber(Code.MIN_SICXE_SIG_OPERAND, Code.MAX_SICXE_OPERAND);
            boolean isIndexed = parser.parseIndexed();

            return new InstructionF3(this, value, tam, isIndexed);
        } // simbol
        else if (Character.isLetter(parser.lexer.peek())) {
            String symbol = parser.parseSymbol();
            boolean isIndexed = parser.parseIndexed();

            return new InstructionF3(this, symbol, tam, isIndexed);
        } // drugace: napaka
        else {
            throw new SyntaxError("symbol name", parser);
        }
    }
    
    @Override
    public String operandToString(Node instruction) {
        InstructionF3 i = (InstructionF3) instruction;
        return i.getTargetAddressingMode() + (i.getSymbol() != null ? i.getSymbol() : Integer.toString(i.getValue()));
    }
}
