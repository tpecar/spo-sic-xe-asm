/*
 * TP, 2017.
 */
package asm.mnemonics;

import asm.code.Node;
import asm.code.SemanticError;
import asm.code.Storage;
import asm.parsing.Parser;
import asm.parsing.SyntaxError;

/**
 * Storage direktiva z dejanskimi podatki (alokacija + inicializacija).
 * 
 * Za pretvorbo operandov direktive skrbi Parser -> parseData
 * 
 * @author tpecar
 */
public class MnemonicSd extends Mnemonic {
    public MnemonicSd(String mnemonic, int opcode, String hint, String desc) {
        super(mnemonic, opcode, hint, desc);
    }
    
    @Override
    public Node parse(Parser parser) throws SyntaxError, SemanticError {
        byte[] data = parser.parseData();
        
        return new Storage(this, data.length, data);
    }
    
    @Override
    public String operandToString(Node directive) {
        Storage s = (Storage) directive;
        
        switch(super.opcode) {
            case Storage.WORD: {
                /* zaenkrat predpostavimo, da WORD direktiva omogoca rezervacijo
                   le ene besede hkrati (ni mozno specificirati tabele) */
                int value = 0;
                return Integer.toString(s.getWord());
            }
            case Storage.BYTE: {
                /* mi ne moremo vedeti v kaksnem formatu je bila byte direktiva
                   podana, tako da predpostavimo heksadecimalno */
                return "X'"+s.dataToString()+"'";
            }
            default: return "[INVALID STORAGE DIRECTIVE]";
        }
    }
}
