package asm.parsing;

/**
 * Podporni razred za predmet Sistemska programska oprema.
 *
 * @author jure
 */
@SuppressWarnings("serial")
public class SyntaxError extends Exception {

    int row;
    int col;

    public SyntaxError(String msg, int row, int col) {
        super(msg);
        this.row = row;
        this.col = col;
    }
    
    public SyntaxError(String object, Parser parser) {
        super(String.format("Invalid character '%c' for %s",
                parser.lexer.peek(), object));
        this.row = parser.lexer.row;
        this.col = parser.lexer.col;
    }

    @Override
    public String toString() {
        String head = "Syntax error at " + Integer.toString(this.row) + ", " + Integer.toString(this.col);
        String message = this.getLocalizedMessage();
        return ((message != null) ? (head + ": " + message) : head) + ".";
    }
}
